<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;

class ListCompanyRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function getListCompanyRoute()
    {
        $response = $this->json('GET', '/api/company');

        /**
         * Asserts
         */
        $response->assertStatus(200);
    }

    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function getListCompanyRoutePerPage()
    {
        $perPage = 10;
        $response = $this->json('GET', sprintf('/api/company?per_page=%s&status=ASC', $perPage));

        /**
         * Asserts
         */
        $response->assertStatus(200);
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals($perPage, $responseData['per_page']);
    }
}
