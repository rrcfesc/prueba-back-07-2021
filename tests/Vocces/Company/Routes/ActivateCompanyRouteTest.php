<?php

declare(strict_types=1);

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class ActivateCompanyRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function postActivateCompanyRoute()
    {
        /** Create a company */
        $faker = \Faker\Factory::create();
        $testCompany = [
            'name'      => $faker->name,
            'email'     => $faker->email,
            'address'   => $faker->address
        ];

        $dataTestCompany = array_merge($testCompany, ['status' => CompanyStatus::DISABLED]);
        $response = $this->json('POST', '/api/company', $testCompany);

        /** Asserts creation */
        $response->assertStatus(201)
            ->assertJsonFragment(array_merge($testCompany, ['status' => 'inactive']));
        $companyData = json_decode($response->getContent(), true);

        $updateCompany = [
            'id' => $companyData['id'],
            'status' => CompanyStatus::ENABLED
        ];
        $response = $this->json('POST', '/api/company/activate', $updateCompany);


        /** Asserts creation */
        $response->assertStatus(201);
    }
}
