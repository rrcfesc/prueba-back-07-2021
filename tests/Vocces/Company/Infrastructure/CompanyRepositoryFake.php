<?php

namespace Tests\Vocces\Company\Infrastructure;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodCreate = false;
    public bool $callMethodFind = false;
    public bool $callMethodUpdate = false;
    public bool $callMethodPaginate = false;

    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
    }

    public function find(string $id)
    {
        $this->callMethodFind = true;
    }

    public function update(Company $company)
    {
        $this->callMethodUpdate = true;
    }

    public function paginate(int $perPage, ?string $status)
    {
        $this->callMethodPaginate = true;
    }
}
