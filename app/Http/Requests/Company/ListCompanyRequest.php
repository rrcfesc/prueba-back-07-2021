<?php

declare(strict_types=1);

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Vocces\Company\Infrastructure\CompanyRepositoryEloquent;

class ListCompanyRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'page' => 'sometimes|nullable|int',
            'per_page' => 'sometimes|nullable|int',
            'status' => [Rule::in(['ASC', 'DESC'])]
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'per_page' => $this->toInt($this->per_page) ?? CompanyRepositoryEloquent::PAGINATE_SIZE
        ]);
    }

    private function toInt($integer)
    {
        return filter_var($integer, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    }
}
