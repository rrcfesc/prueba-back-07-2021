<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Company;

use App\Http\Requests\Company\ActivateCompanyRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyActivator;

class PostActivateCompanyController extends Controller
{
    public function __invoke(ActivateCompanyRequest $request, CompanyActivator $service)
    {
        DB::beginTransaction();
        try {
            $company = $service->handle($request->id, $request->status);
            DB::commit();
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
