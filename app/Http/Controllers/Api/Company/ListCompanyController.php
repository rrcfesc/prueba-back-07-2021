<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\ListCompanyRequest;
use Vocces\Company\Application\CompanyList;

class ListCompanyController extends Controller
{
    public function __invoke(ListCompanyRequest $request, CompanyList $service)
    {
        $data = $service->handle($request->per_page, $request->status);

        return response($data);
    }
}
