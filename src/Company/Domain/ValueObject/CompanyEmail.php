<?php

declare(strict_types=1);

namespace Vocces\Company\Domain\ValueObject;

final class CompanyEmail
{
    private string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function get(): string
    {
        return $this->email;
    }

    public function __toString(): string
    {
        return $this->email;
    }
}
