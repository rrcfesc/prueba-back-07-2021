<?php

namespace Vocces\Company\Domain;

interface CompanyRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return void
     */
    public function create(Company $company): void;

    public function find(string $id);

    public function update(Company $company);

    public function paginate(int $perPage, ?string $status);
}
