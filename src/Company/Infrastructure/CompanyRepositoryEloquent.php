<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    const PAGINATE_SIZE = 15;
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::Create($company->toData());
    }

    public function find(string $id): ModelsCompany
    {
        return ModelsCompany::findOrFail($id);
    }

    public function update(Company $company)
    {
        ModelsCompany::where("id", $company->id())->update($company->toData());
    }

    public function paginate(int $perPage, ?string $status)
    {
        if (null === $status) {
            return ModelsCompany::paginate($perPage);
        } else {
            return ModelsCompany::orderBy('status', $status)->paginate($perPage);
        }
    }
}
