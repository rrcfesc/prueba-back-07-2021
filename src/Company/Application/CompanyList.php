<?php

declare(strict_types=1);

namespace Vocces\Company\Application;

use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyList implements ServiceInterface
{

    private CompanyRepositoryInterface $repository;

    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(int $perPage, ?string $status)
    {
        return $this->repository->paginate($perPage, $status);
    }
}
