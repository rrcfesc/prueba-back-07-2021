<?php

declare(strict_types=1);

namespace Vocces\Company\Application;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyActivator implements ServiceInterface
{
    private CompanyRepositoryInterface $repository;

    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(string $id, int $status)
    {
        $company  = $this->repository->find($id);
        $companyObject = new Company(
            new CompanyId($company->id),
            new CompanyName($company->name),
            CompanyStatus::create($status),
            new CompanyEmail($company->email),
            new CompanyAddress($company->address)
        );

        $this->repository->update($companyObject);


        return $companyObject;
    }
}
